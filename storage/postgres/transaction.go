package postgres

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
	"gitlab.com/har_clone/har_payment_service/storage/repo"
)

type transactionRepo struct {
	db *sqlx.DB
}

func NewTransaction(db *sqlx.DB) repo.TransactionStorageI {
	return &transactionRepo{
		db: db,
	}
}

func (tr *transactionRepo) Create(req *repo.CreateTransactionReq) (*repo.Transaction, error) {
	var result repo.Transaction
	query := `
		INSERT INTO transactions(
			stripe_checkout_session_id,
			amount,
			order_id
		) VALUES($1, $2, $3)
		RETURNING 
			id,
			status, 
			stripe_checkout_session_id,
			amount,
			order_id,
			created_at,
			COALESCE(updated_at,'2006-01-02') as updated_at
	`
	row := tr.db.QueryRow(query, req.StripeCheckoutSessionId, req.Amount, req.OrderId)
	if err := row.Scan(
		&result.Id,
		&result.Status,
		&result.StripeCheckoutSessionId,
		&result.Amount,
		&result.OrderId,
		&result.CreatedAt,
		&result.UpdatedAt,
	); err != nil {
		return &repo.Transaction{}, err
	}
	return &result, nil

}

func (tr *transactionRepo) Get(Id int64) (*repo.Transaction, error) {
	var result repo.Transaction
	query := `
		SELECT 
			id,
			status, 
			stripe_checkout_session_id,
			amount,
			order_id,
			created_at,
			COALESCE(updated_at,'2006-01-02') as updated_at
		FROM transactions WHERE id=$1
	`
	row := tr.db.QueryRow(query, Id)
	if err := row.Scan(
		&result.Id,
		&result.Status,
		&result.StripeCheckoutSessionId,
		&result.Amount,
		&result.OrderId,
		&result.CreatedAt,
		&result.UpdatedAt,
	); err != nil {
		return &repo.Transaction{}, err
	}
	return &result, nil
}

func (tr *transactionRepo) UpdateStatus(req *repo.UpdateTransactionStatus) error {
	query := `
		UPDATE transactions SET 
			status=$1 
		WHERE stripe_checkout_session_id=$2`
	effect, err := tr.db.Exec(query, req.Status, req.StripeCheckoutSessionId)
	if err != nil {
		return err
	}

	if count, _ := effect.RowsAffected(); count == 0 {
		return sql.ErrNoRows
	}
	
	return nil
}
