package repo

import "time"

type Transaction struct {
	Id                      int64
	StripeCheckoutSessionId string
	Amount                  int64
	OrderId                 int64
	Status                  string
	CreatedAt               time.Time
	UpdatedAt               time.Time
}

type UpdateTransactionStatus struct {
	StripeCheckoutSessionId string
	Status                  string
}

type CreateTransactionReq struct {
	StripeCheckoutSessionId string
	Amount                  int64
	OrderId                 int64
}

type TransactionStorageI interface {
	Create(*CreateTransactionReq) (*Transaction, error)
	Get(int64) (*Transaction, error)
	UpdateStatus(*UpdateTransactionStatus) error
}
