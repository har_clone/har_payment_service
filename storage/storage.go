package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/har_clone/har_payment_service/storage/postgres"
	"gitlab.com/har_clone/har_payment_service/storage/repo"
)

type StorageI interface {
	Transaction() repo.TransactionStorageI
}

type storagePg struct {
	transactionRepo repo.TransactionStorageI
}

func NewStoragePg(db *sqlx.DB) StorageI {
	return &storagePg{
		transactionRepo: postgres.NewTransaction(db),
	}
}

func (s *storagePg) Transaction() repo.TransactionStorageI {
	return s.transactionRepo
}
