package service

import (
	pb "gitlab.com/har_clone/har_payment_service/genproto/payment_service"
	"gitlab.com/har_clone/har_payment_service/storage"
)

type PaymentService struct {
	pb.UnimplementedPaymentServiceServer
	storage storage.StorageI
}

func NewPaymentService(strg storage.StorageI) *PaymentService {
	return &PaymentService{
		storage:                           strg,
		UnimplementedPaymentServiceServer: pb.UnimplementedPaymentServiceServer{},
	}
}


