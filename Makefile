POSTGRES_HOST=localhost
POSTGRES_PORT=5432
POSTGRES_USER=postgres
POSTGRES_PASSWORD=postgres
POSTGRES_DATABASE=har_payment_service_db

CURRENT_DIR=$(shell pwd)

-include .env
  
DB_URL="postgresql://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@$(POSTGRES_HOST):$(POSTGRES_PORT)/$(POSTGRES_DATABASE)?sslmode=disable"

run:
	go run cmd/main.go

migrateup:
	migrate -path migrations -database "$(DB_URL)" -verbose up

migrateup1:
	migrate -path migrations -database "$(DB_URL)" -verbose up 1

migratedown:
	migrate -path migrations -database "$(DB_URL)" -verbose down

migratedown1:
	migrate -path migrations -database "$(DB_URL)" -verbose down 1
	
migrate_file:
	migrate create -ext sql -dir migrations/ -seq alter_some_table
proto-gen:
	rm -rf genproto
	./scripts/gen-proto.sh ${CURRENT_DIR}

swag-init:
	swag init -g api/api.go -o api/docs

.PHONY: run migrateup migratedown